rule fastq:
    message:
        "Extracting unmapped reads..."
    input:
        bam="results/bam/{sample}.bam"
    output:
        fq1="results/unmapped_fastq/{sample}_1_unmapped.fastq",
        fq2="results/unmapped_fastq/{sample}_2_unmapped.fastq"
    log:
        "logs/samtools/fastq/{sample}.log"
    threads:4
    conda:
       "../envs/yourenv.yaml"
    shell:
        """
        samtools view --threads={threads} -b -f4 {input.bam} > results/bam/{wildcards.sample}_unmapped.bam 2> {log} 
        samtools sort -n results/bam/{wildcards.sample}_unmapped.bam -o results/bam/{wildcards.sample}_sorted.bam 2>> {log}
        bedtools bamtofastq -i results/bam/{wildcards.sample}_sorted.bam -fq {output.fq1} -fq2 {output.fq2} 2>> {log}
        """
    

