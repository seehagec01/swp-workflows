rule refindex:
        input:
            "results/reference/reference.fa"
        params:
            prefix=config["index"]
        output:
            "results/reference/index.bt2"
        threads:4
        conda:
            "../envs/yourenv.yaml"
        shell:
            "bowtie2-build {input} {params.prefix} > {output}"



rule map:
   input:
        "results/reference/index.bt2",
        "results/trimmed/{sample}_forward_paired.fq.gz",
        "results/trimmed/{sample}_reverse_paired.fq.gz"
   params:
        prefix=config["index"],
        bowtieparams=config["bowtieparams"]
   output:
        "results/sam/{sample}.sam"
   log:
        "workflow/report/mapping/{sample}.log"
   threads:4
   conda:
       "../envs/yourenv.yaml"
   shell:
        "bowtie2 {params.bowtieparams} -x {params.prefix} --threads={threads} -1 {input[1]} -2 {input[2]}  > {output} 2> {log}"
