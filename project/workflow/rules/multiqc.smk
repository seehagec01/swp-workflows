rule multiqc:
    message:
        "Creating MultiQC report..."
    input:
        results="results/",
        fastp="results/fastp/report/"
    output:
        report="results/multiqc/multiqc.html"
    conda:
        "../envs/yourenv.yaml"
    shell:
        """
        multiqc -n multiqc.html {input.results} {input.fastp} 
        mv multiqc.html {output.report}
        mv multiqc_data/ results/
        """