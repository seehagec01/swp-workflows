rule IVA:
    message: 
        "Genome Assembly..."
    input:
        fq1="results/unmapped_fastq/{sample}_1_unmapped.fastq",
        fq2="results/unmapped_fastq/{sample}_2_unmapped.fastq"
    output:
        IVA_dir=directory("results/IVA/{sample}/")
    log:
        "logs/IVA/{sample}.log"
    threads: 16
    conda:
       "../envs/yourenv.yaml"
    shell:
        "iva -f {input.fq1} -r {input.fq2} {output.IVA_dir} --threads {threads} 2> {log}"
        
    