rule build:
    message:
        "Building Kraken database..."
    output:
        database="krakendb"
    threads: 64
    conda:
        "../envs/yourenv.yaml"
    log:
        "logs/kraken/build/krakendb.log"
    shell:
        """
        kraken2-build --download-taxonomy --db {output} 2> {log}
        kraken2-build --download-library archaea --db {output} 2>>{log}
        kraken2-build --download-library bacteria --db {output} 2>> {log}
        kraken2-build --download-library plasmid --db {output} 2>> {log}
        kraken2-build --download-library viral --db {output} 2>> {log}
        kraken2-build --download-library human --db {output} 2>> {log}
        kraken2-build --build --db {output} --threads {threads} 2>> {log}
        """ 

rule report_trimmedreads:
    message:
        "Creating Kraken report using trimmed reads..."
    input:
        database=config["kraken_db"],
        sample=["results/fastp/trimmed/{sample}.1.fastq", "results/fastp/trimmed/{sample}.2.fastq"]
    output:
        output="results/kraken/{sample}_trimmed.kraken",
        report="results/kraken/{sample}_trimmed.kreport2"
    threads: 8
    conda:
        "../envs/yourenv.yaml"
    log:
        "logs/kraken/report/{sample}_trimmed.log"
    shell:
        "kraken2 --threads {threads} --db {input.database} --paired {input.sample} --report {output.report} > {output.output} 2> {log}"

rule report_unmappedreads:
    message: 
        "Creating Kraken report using unmapped reads..."
    input:
        database="/buffer/ag_bsc/pmsb_workflows_2022/kraken_standard_db/standard_db",
        sample=["results/unmapped_fastq/{sample}_1_unmapped.fastq", "results/unmapped_fastq/{sample}_2_unmapped.fastq"]
    output:
        output="results/kraken/{sample}_unmapped.kraken",
        report="results/kraken/{sample}_unmapped.kreport2"
    threads: 8
    conda:
        "../envs/yourenv.yaml"
    log:
        "logs/kraken/report/{sample}_unmapped.log"
    shell:
        "kraken2 --threads {threads} --db {input.database} --paired {input.sample} --report {output.report} > {output.output} 2> {log}"