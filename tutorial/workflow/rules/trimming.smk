rule trimming:
    input:
         rt1 = lambda wildcards: samples.at[wildcards.sample,'fq1'] if wildcards.sample in samples.index else ' ',
         rt2 = lambda wildcards: samples.at[wildcards.sample,'fq2'] if wildcards.sample in samples.index else ' '
    params:
        adapter=config["adapter"]
    output:
        "results/trimmed/{sample}_forward_paired.fq.gz",
        "results/trimmed/{sample}_forward_unpaired.fq.gz",
        "results/trimmed/{sample}_reverse_paired.fq.gz",
        "results/trimmed/{sample}_reverse_unpaired.fq.gz"
    conda:
        "../envs/yourenv.yaml"
    log:
         "workflow/report/trimming/{sample}.log"
    shell:
        "trimmomatic PE {input[0]} {input[1]} {output} ILLUMINACLIP:{params.adapter}:2:30:10:2:True LEADING:3 TRAILING:3 MINLEN:36"
