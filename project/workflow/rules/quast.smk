rule quast_IVA:
    message:
        "Creating QUAST report for IVA contigs..."
    input:
        contigs="results/IVA/{sample}/contigs.fasta"
    output:
        quast_dir=directory("results/quast_IVA/{sample}")
    conda:
        "../envs/quast.yaml"
    threads: 4
    log:
        "logs/quast_IVA/{sample}.log"
    shell:
        "quast -o {output.quast_dir} -t {threads} {input.contigs} 2> {log}"


rule quast_shiver:
    message:
        "Creating QUAST report for shiver consensus genome..."
    input:
        consensus_genome="results/shiver/{sample}/{sample}.fasta",
        quast_ref_genome=config["viral_reference_alignment"]
        #gene_features=config["viral_reference_gene_features"]
    output:
        quast_dir=directory("results/quast_shiver/{sample}")
    conda:
        "../envs/quast.yaml"
    threads: 4
    log:
        "logs/quast_shiver/{sample}.log"
    shell:
        "quast -r {input.quast_ref_genome} -o {output.quast_dir} {input.consensus_genome} > {log} 2>&1"