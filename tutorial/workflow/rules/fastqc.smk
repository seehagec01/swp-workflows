#map_dict = {'1':'fq1', '2':'fq2'}

#rule rawfastqc:
  #input:
      #rf1 = lambda wildcards: samples.at[wildcards.sample,map_dict[wildcards.i]] if wildcards.sample in samples.index else ' '
      #rf2 = lambda wildcards: samples.at[wildcards.sample,map_dict[wildcards.i]] if wildcards.sample in samples.index else ' '
  #output:
      #html="results/fastqc/{sample}_{i}_fastqc.html",
      #zip="results/fastqc/{sample}_{i}_fastqc.zip"
  #conda:
      #"../envs/yourenv.yaml"
  #log:
      #"workflow/report/fastqc/{sample}_{i}.log"
  #wrapper:
      #"v1.3.1/bio/fastqc"

rule rawfastqc1:
  input:
      rf1 = lambda wildcards: samples.at[wildcards.sample,'fq1'] if wildcards.sample in samples.index else ' '
  output:
      html="results/fastqc/raw/{sample}_1_fastqc.html",
      zip="results/fastqc/raw/{sample}_1_fastqc.zip"
  conda:
      "../envs/yourenv.yaml"
  log:
      "workflow/report/fastqc/{sample}.log"
  wrapper:
      "v1.3.1/bio/fastqc"


rule rawfastqc2:
  input:
      rf2 = lambda wildcards: samples.at[wildcards.sample,'fq2'] if wildcards.sample in samples.index else ' '
  output:
      html="results/fastqc/raw/{sample}_2_fastqc.html",
      zip="results/fastqc/raw/{sample}_2_fastqc.zip"
  conda:
      "../envs/yourenv.yaml"
  log:
      "workflow/report/fastqc/{sample}.log"
  wrapper:
      "v1.3.1/bio/fastqc"


rule trimmedfastqc1:
    input:
        "results/trimmed/{sample}_forward_paired.fq.gz",
        "results/trimmed/{sample}_reverse_paired.fq.gz"
    output:
        html="results/trimmed_fastqc/{sample}_1_fastqc.html",
        zip="results/trimmed_fastqc/{sample}_1_fastqc.zip"
    conda:
        "../envs/yourenv.yaml"
    log:
        "workflow/report/fastqc/trimmed_{sample}.log"
    wrapper:
        "v1.3.1/bio/fastqc"


rule trimmedfastqc2:
    input:
        "results/trimmed/{sample}_forward_paired.fq.gz",
        "results/trimmed/{sample}_reverse_paired.fq.gz"
    output:
        html="results/trimmed_fastqc/{sample}_2_fastqc.html",
        zip="results/trimmed_fastqc/{sample}_2_fastqc.zip"
    conda:
        "../envs/yourenv.yaml"
    log:
        "workflow/report/fastqc/trimmed_{sample}.log"
    wrapper:
        "v1.3.1/bio/fastqc"
