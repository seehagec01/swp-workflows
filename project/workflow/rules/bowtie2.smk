rule refindex:
    message:
        "Building mapping index..."
    input:
        prefix=config["human_reference"]
    params:
        prefix=config["index"]
    output:
        "results/reference/index.bt2" 
    log:
        "logs/bowtie2/refindex/index.1.log"
    threads:4
    conda:
        "../envs/yourenv.yaml"
    shell:
        "bowtie2-build {input} {params.prefix} > {output}"


rule map:
    message:
        "Mapping reads to human reference..."
    input:
        idx=multiext(
            "results/reference/index",
            ".1.bt2",
            ".2.bt2",
            ".3.bt2",
            ".4.bt2",
            ".rev.1.bt2",
            ".rev.2.bt2",
        ),
        sample=["results/fastp/trimmed/{sample}.1.fastq", "results/fastp/trimmed/{sample}.2.fastq"]
    params:
        extra=config["bowtie_params"]
    output:
        "results/bam/{sample}.bam"
    threads:4
    conda:
        "../envs/yourenv.yaml"
    log:
        "logs/bowtie2/map/{sample}.log"
    wrapper:
        "v1.3.2/bio/bowtie2/align"


