rule aggregate:
         input:
            "results/stats/ERR024604.stats.txt"
         output:
            "results/aggregate/mappedcounts.csv"
         threads:4
         conda:
            "../envs/yourenv.yaml"
         script:
            "../scripts/aggregation.py"
