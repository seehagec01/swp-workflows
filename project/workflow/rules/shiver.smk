rule shiver_initialization:
    message: 
        "Initializing shiver..."
    input:
        reference_alignment=config["viral_reference_alignment"],
        adapters=config["viral_sequencing_adapters"],
        primers=config["viral_sequencing_primers"],
        shiver_config=config["shiver_config_file"]
    output:
        initialization_dir=directory("results/shiver/{sample}/{sample}_shiver_init_dir")
    conda:
        "../envs/shiver.yaml"
    log:
        "logs/shiver/{sample}/{sample}_init.log"   
    shell:
        "shiver_init.sh {output.initialization_dir} {input.shiver_config} {input.reference_alignment} {input.adapters} {input.primers} 2> {log}"


rule shiver_align_contigs_to_reference:
    message:
        "Aligning contigs to reference..."
    input:
        initialization_dir=rules.shiver_initialization.output.initialization_dir,
        contigs_file="results/IVA/{sample}/contigs.fasta",
        shiver_config=config["shiver_config_file"]
    output:
        blast_hits="results/shiver/{sample}/{sample}.blast",
        aligned_contigs_raw="results/shiver/{sample}/{sample}_raw_wRefs.fasta"
        #aligned_contigs_cut="results/shiver/{sample}/{sample}_cut_wRefs.fasta"
    conda:
        "../envs/shiver.yaml"
    log:
        "logs/shiver/{sample}/{sample}_shiver_align_contigs.log"
    shell:
        """
        mkdir shiver_temp_{wildcards.sample}
        cd shiver_temp_{wildcards.sample}
        shiver_align_contigs.sh ../{input.initialization_dir} ../{input.shiver_config} ../{input.contigs_file} ../results/shiver/{wildcards.sample}/{wildcards.sample} > ../{log} 2>&1
        cd ..
        rm -r shiver_temp_{wildcards.sample}
        """


rule shiver_read_remapping:
    message:
        "Mapping..."
    input:
        initialization_dir=rules.shiver_initialization.output.initialization_dir,
        contigs="results/IVA/{sample}/contigs.fasta",
        aligned_contigs_raw=rules.shiver_align_contigs_to_reference.output.aligned_contigs_raw,
        blast_hits=rules.shiver_align_contigs_to_reference.output.blast_hits,
        #aligned_contigs_cut=rules.shiver_align_contigs_to_reference.output.aligned_contigs_cut,
        forward_read="results/unmapped_fastq/{sample}_1_unmapped.fastq",
        reverse_read="results/unmapped_fastq/{sample}_2_unmapped.fastq",
        shiver_config=config["shiver_config_file"]
    output:
        ref_seqs="results/shiver/{sample}/{sample}_ref.fasta",
        bam="results/shiver/{sample}/{sample}.bam",
        base_freqs="results/shiver/{sample}/{sample}_BaseFreqs.csv",
        base_freqs_global_aln="results/shiver/{sample}/{sample}_BaseFreqs_ForGlobalAln.csv",
        coords="results/shiver/{sample}/{sample}_coords.csv",
        insert_size_dist="results/shiver/{sample}/{sample}_InsertSizeCounts.csv",
        consensus_genome="results/shiver/{sample}/{sample}_remap_consensus_MinCov_15_30.fasta"
    conda:
        "../envs/shiver.yaml"
    log:
        "logs/shiver/{sample}/{sample}_shiver_map_reads.log"
    shell:
        """
        mkdir shiver_temp_{wildcards.sample}
        cd shiver_temp_{wildcards.sample}
        shiver_map_reads.sh ../{input.initialization_dir} ../{input.shiver_config} ../{input.contigs} ../results/shiver/{wildcards.sample}/{wildcards.sample} ../{input.blast_hits} ../{input.aligned_contigs_raw} ../{input.forward_read} ../{input.reverse_read} > ../{log} 2>&1
        cd ..
        rm -r shiver_temp_{wildcards.sample}
        """

rule tidy_shiver_output:
    message:
        "Tidying up..."
    input:
        consensus_genome="results/shiver/{sample}/{sample}_remap_consensus_MinCov_15_30.fasta"
    output:
        tidy_consensus_genome="results/shiver/{sample}/{sample}.fasta"
    conda:
        "../envs/shiver.yaml"
    log:
        "logs/shiver/{sample}/{sample}_shiver_tidy_contigs.log"
  
    shell:
        """
        seqkit range -r 1:1 -w 0 {input} | sed 's/?/N/g' > {output} 2> {log}
        """