rule multiqc:
    input:
        "results/qualimap/{sample}/qualimapReport.html",
        "results/qualimap/{sample}/genome_results.txt",
        "results/trimmed_fastqc/{sample}_1_fastqc.html",
        "results/trimmed_fastqc/{sample}_2_fastqc.html",
        "results/trimmed_fastqc/{sample}_1_fastqc.zip",
        "results/trimmed_fastqc/{sample}_2_fastqc.zip"
    output:
        "results/multiqc/multiqc.html"
    shell:
        """
        multiqc -n multiqc.html {input}
        mv multiqc.html {output}
        """
