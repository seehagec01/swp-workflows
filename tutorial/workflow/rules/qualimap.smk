rule qualimap:
    input:
        "results/bam_sorted/{sample}.sorted.bam"
    output:
        "results/qualimap/{sample}/qualimapReport.html"
    conda:
        "../envs/yourenv.yaml"
    log:
        "workflow/report/qualimap/{sample}.log"
    shell:
        "qualimap bamqc -bam {input} -outdir results/qualimap/{wildcards.sample} 2> {log}"
