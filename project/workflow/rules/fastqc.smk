rule rawfastqc1:
  input:
      rf1 = "Data/SARS-Cov-2/{sample}_1.fastq.gz"
  output:
      html="results/fastqc/raw/{sample}_1_fastqc.html",
      zip="results/fastqc/raw/{sample}_1_fastqc.zip"
  conda:
      "../envs/yourenv.yaml"
  log:
      "logs/fastqc/{sample}.log"
  wrapper:
      "v1.3.1/bio/fastqc"


rule rawfastqc2:
  input:
      rf2 = "Data/SARS-Cov-2/{sample}_2.fastq.gz"
  output:
      html="results/fastqc/raw/{sample}_2_fastqc.html",
      zip="results/fastqc/raw/{sample}_2_fastqc.zip"
  conda:
      "../envs/yourenv.yaml"
  log:
      "logs/fastqc/{sample}.log"
  wrapper:
      "v1.3.1/bio/fastqc"


rule trimmedfastqc1:
    input:
        sample=["results/fastp/trimmed/{sample}.1.fastq","results/fastp/trimmed/{sample}.2.fastq"]
    output:
        html="results/fastqc/trimmed/{sample}_1_fastqc.html",
        zip="results/fastqc/trimmed/{sample}_1_fastqc.zip"
    conda:
        "../envs/yourenv.yaml"
    log:
        "logs/fastqc/trimmed_{sample}.log"
    wrapper:
        "v1.3.1/bio/fastqc"


rule trimmedfastqc2:
    input:
        sample=["results/fastp/trimmed/{sample}.1.fastq","results/fastp/trimmed/{sample}.2.fastq"]
    output:
        html="results/fastqc/trimmed/{sample}_2_fastqc.html",
        zip="results/fastqc/trimmed/{sample}_2_fastqc.zip"
    conda:
        "../envs/yourenv.yaml"
    log:
        "logs/fastqc/trimmed_{sample}.log"
    wrapper:
        "v1.3.1/bio/fastqc"
