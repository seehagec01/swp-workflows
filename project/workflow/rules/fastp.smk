rule fastp:
    message: 
        "Quality controlling raw data..."
    input:
        sample=["Data/SARS-Cov-2/{sample}_1.fastq.gz", "Data/SARS-Cov-2/{sample}_2.fastq.gz"]
    output:
        trimmed=["results/fastp/trimmed/{sample}.1.fastq", "results/fastp/trimmed/{sample}.2.fastq"],
        html="results/fastp/report/{sample}.html",
        json="results/fastp/report/{sample}.json"
    log:
        "logs/fastp/{sample}.log"
    conda:
        "../envs/yourenv.yaml"
    params:
        extra=config["fastp_params"]
    threads: 4
    wrapper:
        "v1.3.2/bio/fastp"