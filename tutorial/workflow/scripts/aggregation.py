import pandas as pd
#import numpy as np

#SAMPLES=("ERR024604", "ERR024605", "ERR024606", "ERR024607", "ERR024608", "ERR024609")


df04=pd.read_table("results/stats/ERR024604.stats.txt", header=None, usecols=[2])
df05=pd.read_table("results/stats/ERR024605.stats.txt", header=None, usecols=[2])
df06=pd.read_table("results/stats/ERR024606.stats.txt", header=None, usecols=[2])
df07=pd.read_table("results/stats/ERR024607.stats.txt", header=None, usecols=[2])
df08=pd.read_table("results/stats/ERR024608.stats.txt", header=None, usecols=[2])
df09=pd.read_table("results/stats/ERR024609.stats.txt", header=None, usecols=[2])

index = []
for x in range(141):
    indexname = 'chr' + str(x)
    index.append(indexname)

d={'sequence': index}
i=pd.DataFrame(d)


frames=[i, df04, df05, df06, df07, df08, df09]
result=pd.concat(frames, axis=1, ignore_index=True, names=['sequence','sample04', 'sample05', 'sample06', 'sample07', 'sample08', 'sample09'])
result.columns=['sequence','ERR024604', 'ERR024605', 'ERR024606', 'ERR024607', 'ERR024608', 'ERR024609']


import os
os.makedirs('results/aggregate', exist_ok=True)
snakemake.output= result.to_csv('results/aggregate/mappedcounts.csv')


#print(result)
