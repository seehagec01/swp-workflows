rule convert:
       input:
            "results/sam/{sample}.sam"
       output:
            "results/bam/{sample}.bam"
       threads:4
       conda:
            "../envs/yourenv.yaml"
       shell:
            "samtools view -S -b --threads={threads} {input} > {output}"


rule sort:
    input:
        "results/bam/{sample}.bam"
    output:
        "results/bam_sorted/{sample}.sorted.bam"
    log:
        "workflow/report/sort/{sample}.log"
    threads:4
    conda:
        "../envs/yourenv.yaml"
    shell:
        "samtools sort --threads={threads} {input} > {output} 2> {log}"


rule index:
    input:
        "results/bam_sorted/{sample}.sorted.bam"
    output:
        "results/bam_sorted/{sample}.sorted.bam.bai"
    threads:4
    conda:
        "../envs/yourenv.yaml"
    shell:
        "samtools index -@ {threads} {input}"


rule stats:
    input:
        "results/bam_sorted/{sample}.sorted.bam",
        "results/bam_sorted/{sample}.bam.bai"
    output:
        "results/stats/{sample}.stats.txt"
    conda:
        "../envs/yourenv.yaml"
    shell:
        "samtools idxstats {input[0]} > {output}"
